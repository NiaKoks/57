const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const arrFrontsum = tasks.reduce((accum, currentVal) => {
    if(currentVal.category === 'Frontend') {
        accum += currentVal.timeSpent
    }
    return accum
}, 0);
console.log(arrFrontsum);

const arrBugFixes  = tasks.reduce((acc, currentVal)=>{
    if(currentVal.type === 'bug'){
        acc += currentVal.timeSpent
    }
    return acc
}, 0);
console.log(arrBugFixes);

const arrUItask = tasks.filter(tasks => tasks.title.includes('UI')).length;
console.log(arrUItask)

const arrFrontBack = tasks.reduce((accum, task) => {
    if(task.category==='Frontend'){
        accum.Frontend++;
    }else if(task.category==='Backend'){
        accum.Backend++;
    }
    return accum;
}, {Frontend: 0, Backend: 0});

console.log(arrFrontBack);

const arrMoreThanFour = tasks.map(task => {
    if(task.timeSpent >= 4) {
        return {title: task.title, category: task.category}
    }
}).filter(task => {
    if(task !== undefined) {
        return task
    }
});

console.log(arrMoreThanFour)