import React from "react";

const AddForm = props =>{
    return(
        <div className="add-task">
            <input value={props.taskName} onChange={(e)=> props.taskNameHandler(e.target.value) } type="text"  className="add-inp" placeholder="Name of outlay"/>
            <input value={props.taskCost} onChange={(e)=> props.taskCostHandler(e.target.value)} type="number" className="add-cost"/>
            <p className="valute">KGS</p>
            <button onClick={props.addTaskHandler} className="add-btn">Add</button>
        </div>
    )
}
export default AddForm;