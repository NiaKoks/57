import React from "react";

const Outlay = props =>{
    console.log(props);
    return(
        <div className="task-field">
            <p className="product-name">{props.name}</p>
            <p className="product-cost">{props.cost} KGS</p>
            <button onClick={props.remove} className="del-btn">Del</button>
        </div>
    )
}
export default Outlay;