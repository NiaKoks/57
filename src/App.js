import React, { Component } from 'react';
import './App.css';
import AddForm from './components/AddForm.js';
import Outlay from './components/Outlay.js'

class App extends Component {
  state = {
    tasks:[],
    taskName: '',
    taskCost: '',
    totalPrice: 0
  };

  taskNameChangeHandler = (value)=> {
    this.setState({taskName: value})
  };

  taskCostChangHandler = (value) => {
    if (parseInt(value) >= 0) this.setState({taskCost: value})
    else this.setState({taskCost: ""})
  };

  addTask = () => {
    let newTask = {task: this.state.taskName, cost: this.state.taskCost};
    let tasks = [...this.state.tasks, newTask];
    let totalPrice = tasks.reduce((total, task) => total + parseInt(task.cost), 0);
    this.setState({tasks, totalPrice})
  };

  delTask = (name) => {
    let tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.task === name)
    tasks.splice(index, 1);
    let totalPrice = tasks.reduce((total, task) => total + parseInt(task.cost), 0);
    this.setState({tasks, totalPrice})
  };

  render() {
    return (
        <div className="planner">
          <h2>My expenses</h2>
          <AddForm
              taskName={this.state.taskName}
              taskNameHandler={this.taskNameChangeHandler}
              taskCost={this.state.taskCost}
              taskCostHandler={this.taskCostChangHandler}
              addTaskHandler={this.addTask}
          />
          {this.state.tasks.map((task, key) => <Outlay remove = {() => this.delTask(task.task)} key={key} name={task.task} cost={task.cost} />)}
          <b>Всего: {this.state.totalPrice} KGS</b>
        </div>
    );
  }
}

export default App;
